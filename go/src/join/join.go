package main

import (
    "bufio"
    "errors"
    "fmt"
    "log"
    "net"
    "os"
    "strconv"
    "zrunner"
)

func parse_args() (host string, port int, name string, key string, err error) {
    args := os.Args[1:]
    if len(args) != 4 {
        return "", 0, "", "", errors.New("Usage: ./run host port botname botkey")
    }
    host = args[0]
    port, err = strconv.Atoi(args[1])
    if err != nil {
        return "", 0, "", "", errors.New(fmt.Sprintf("Could not parse port value to integer: %v\n", args[1]))
    }
    name = args[2]
    key = args[3]

    return
}

func log_and_exit(err error, msg interface{}) {
    log.Println("Error occured while ", msg, "; Error: ", err)
    os.Exit(1)
}

func connect(host string, port int) (conn net.Conn, err error) {
    conn, err = net.Dial("tcp", fmt.Sprintf("%s:%d", host, port))
    return
}

func main() {

    host, port, name, key, err := parse_args()

    if err != nil {
        log_and_exit(err, "Arg parse")
    }

    fmt.Println("Connecting with parameters:")
    fmt.Printf("host=%v, port=%v, bot name=%v, key=%v\n", host, port, name, key)

    conn, err := connect(host, port)

    if err != nil {
        log_and_exit(err, "Connection to server")
    }

    defer conn.Close()

    bot := zrunner.Bot{Reader: bufio.NewReader(conn),
        Writer:             bufio.NewWriter(conn),
        Name:               name,
        Key:                key,
        MaxForce:           2.7,
        BrakeThrottle:      0.2,
        MinThrottle:        0.4,
        SafeSpeedModifier:  8,
        FarSeer:            20,
        BrakeDecseleration: 0.987,
        MaxTestTicks:       700,
        Create:             false,
    }

    err = bot.Run()
}

package zrunner

import (
	"bufio"
	//"runtime/debug"
	"encoding/json"
	"fmt"
	"log"
	"math"
	"os"
	//	"reflect"
	"time"
	"zrunner/msg"
)

type Bot struct {
	// Communication
	Reader          *bufio.Reader
	Writer          *bufio.Writer
	Name, Key       string
	TrackName, Pass string
	NumCars         int
	Create          bool
	// Info
	track    msg.Track
	raceInfo msg.RaceInfo
	cars     []msg.Car
	color    string
	myDimm   msg.CarDimmensions
	// Logs & dumps
	log  *log.Logger
	dump *os.File
	// Previous tick data
	lastPosition msg.CarPosition
	speed        float64
	acseleration float64
	cForce       float64
	angleForce   float64
	// Status
	crashed bool
	Ended   bool
	// Pre race test parameters
	MaxTestTicks int
	acsTest      bool
	brakeTest    bool
	cornerTest   bool
	// Controll parameters
	MaxForce           float64
	BrakePower         float64
	BrakeThrottle      float64
	MinThrottle        float64
	throttle           float64
	safeSpeed          float64
	SafeSpeedModifier  float64
	FarSeer            int
	BrakeDecseleration float64
}

func (bot *Bot) read_msg() (line string, err error) {
	line, err = bot.Reader.ReadString('\n')
	if err != nil {
		bot.log.Println(fmt.Sprintf("Error during reading from server"))
	}
	return
}

func (bot *Bot) write_msg(msgtype string, data interface{}) (err error) {
	m := make(map[string]interface{})
	m["msgType"] = msgtype
	m["data"] = data
	var payload []byte
	payload, err = json.Marshal(m)
	//bot.log.Printf("Sending message [%s] %s", msgtype, string(payload))
	if msgtype == "createRace" {
		bot.log.Printf("Sending json: %s", string(payload))
	}
	_, err = bot.Writer.Write([]byte(payload))
	if err != nil {
		return
	}
	_, err = bot.Writer.WriteString("\n")
	if err != nil {
		return
	}
	bot.Writer.Flush()
	return
}

func (bot *Bot) send_create() (err error) {
	data := msg.MsgCreate{
		BotId: msg.BotId{Name: bot.Name, Key: bot.Key},
		Track: bot.TrackName,
		Pass:  bot.Pass,
		Cars:  bot.NumCars,
	}
	bot.log.Println("Just joining")
	bot.log.Printf("Creating game on track: %s", bot.TrackName)
	err = bot.write_msg("createRace", data)
	return
}

func (bot *Bot) send_join() (err error) {
	data := make(map[string]string)
	data["name"] = bot.Name
	data["key"] = bot.Key
	err = bot.write_msg("join", data)
	return
}

func (bot *Bot) send_ping() (err error) {
	err = bot.write_msg("ping", make(map[string]string))
	return
}

func (bot *Bot) send_throttle() (err error) {
	if bot.throttle < 0 {
		bot.throttle = 0.01
	}
	if bot.throttle > 1 {
		bot.throttle = 1
	}
	err = bot.write_msg("throttle", bot.throttle)
	return
}

func (bot *Bot) send_switch(dir string) (err error) {
	err = bot.write_msg("switchLane", dir)
	return
}

func (bot *Bot) calculateDistanceToTurn(myCar msg.CarPosition) (distance, radius float64) {
	myIndex := myCar.Position.Index
	if bot.track.Pieces[myIndex].Radius > 0 {
		radius = bot.track.Pieces[myIndex].LaneRadius[myCar.Position.Lane.End]
		return
	}
	distance = bot.track.Pieces[myCar.Position.Index].Length - myCar.Position.InPiece
	track_len := bot.track.Length
	for i := myIndex + 1; i < track_len; i++ {
		if bot.track.Pieces[i%track_len].Radius > 0 {
			radius = bot.track.Pieces[i%track_len].LaneRadius[myCar.Position.Lane.End]
			break
		}
		distance += bot.track.Pieces[i%track_len].Length
	}
	return
}

func (bot *Bot) onJoin() {
	bot.log.Printf("Joined")
	bot.send_ping()
}

func (bot *Bot) onGameStart() {
	bot.log.Printf("Game started")
	bot.send_ping()
}

func (bot *Bot) onYourCar(line string) {
	var yourCar msg.EnvelopeYourCar
	err := json.Unmarshal([]byte(line), &yourCar)
	if err != nil {
		bot.log.Println("Error parsing car definition message", err)
		bot.send_ping()
		return
	}

	bot.color = yourCar.Data.Color
}

func (bot *Bot) onGameInit(line string) {
	var gameInit msg.EnvelopeGameInit
	err := json.Unmarshal([]byte(line), &gameInit)
	if err != nil {
		bot.log.Println("Error parsing car position message", err)
		bot.send_ping()
		return
	}

	bot.log.Println("Got track configuration")
	bot.track = gameInit.Data.Race.Track
	bot.raceInfo = gameInit.Data.Race.Info
	bot.cars = gameInit.Data.Race.Cars
	bot.lastPosition.Position.Index = -1
	bot.track.Length = len(bot.track.Pieces)

	for _, car := range bot.cars {
		if car.Id.Name == bot.Name && car.Id.Color == bot.color {
			bot.myDimm = car.Dimensions
			bot.log.Printf("My dimmensions: len=%f, guide=%f", car.Dimensions.Length, car.Dimensions.Guide)
		}
	}

	for i := 0; i < bot.track.Length; i++ {
		piece := bot.track.Pieces[i]
		bot.track.Pieces[i].LaneLengths = make(map[int]float64)
		bot.track.Pieces[i].LaneRadius = make(map[int]float64)
		if piece.Radius == 0 {
			for _, lane := range bot.track.Lanes {
				bot.track.Pieces[i].LaneLengths[lane.Index] = piece.Length
				bot.track.Pieces[i].LaneRadius[lane.Index] = 0
			}
		} else {
			var radius float64
			for _, lane := range bot.track.Lanes {
				radius = piece.Radius
				if math.Signbit(piece.Angle) {
					radius += lane.Offset
				} else {
					radius -= lane.Offset
				}

				bot.track.Pieces[i].LaneLengths[lane.Index] = math.Pi * radius * math.Abs(piece.Angle) / 180
				bot.track.Pieces[i].LaneRadius[lane.Index] = radius
			}
		}
	}

	bot.send_ping()
}

func (bot *Bot) onGameEnd(line string) {
	var gameEnd msg.EnvelopeGameEnd
	err := json.Unmarshal([]byte(line), &gameEnd)
	if err != nil {
		bot.log.Println("Error parsing gameEnd message", err)
		return
	}

	bot.log.Printf("Game ended")
	bot.log.Print("Results:")
	bot.log.Print("Name; Laps; Ticks; Time")
	for _, car := range gameEnd.Data.Results {
		bot.log.Printf("%s; %d; %d; %f", car.Car.Name, car.Result.Laps, car.Result.Ticks, float64(car.Result.Millis)/1000)
	}
	bot.log.Println()
	bot.log.Print("Best Laps:")
	bot.log.Print("Name; Lap; Ticks; Time")
	for _, car := range gameEnd.Data.BestLaps {
		bot.log.Printf("%s; %d; %d; %f", car.Car.Name, car.Result.Lap, car.Result.Ticks, float64(car.Result.Millis)/1000)
	}
	bot.Ended = true
}

func (bot *Bot) onCrash(line string) {
	var crash msg.EnvelopeCrash
	err := json.Unmarshal([]byte(line), &crash)
	if err != nil {
		bot.log.Println("Error parsing crash message", err)
		bot.send_ping()
		return
	}
	if crash.Data.Name == bot.Name {
		bot.dump.Write([]byte("\"Crash\"\n"))
		bot.log.Printf("I'm crashed! Waiting for the spawn. (tick %d)", crash.Tick)
		bot.MaxForce = math.Min(bot.cForce*4, bot.MaxForce*0.95)
		bot.log.Printf("New maxForce = %f", bot.MaxForce)
		bot.crashed = true
		bot.lastPosition.Angle = 0
	} else {
		bot.log.Printf("%s crashed", crash.Data.Name)
	}
	bot.send_ping()
}

func (bot *Bot) onSpawn(line string) {
	var spawn msg.EnvelopeSpawn
	err := json.Unmarshal([]byte(line), &spawn)
	if err != nil {
		bot.log.Println("Error parsing spawn message", err)
		bot.send_ping()
		return
	}
	if spawn.Data.Name == bot.Name {
		bot.log.Println("I'm spawned! Start engines!")
		bot.crashed = false
		bot.throttle = 0.8
	} else {
		bot.log.Printf("%s spawned", spawn.Data.Name)
	}
	bot.send_throttle()
}

func (bot *Bot) onLapFinish(line string) {
	var lap msg.EnvelopeLapFinish
	err := json.Unmarshal([]byte(line), &lap)
	if err != nil {
		bot.log.Println("Error parsing lapFinish message", err)
		bot.send_ping()
		return
	}
	bot.log.Printf("%s finished", lap.Data.Car.Name)
	if lap.Data.Car.Name == bot.Name {
		bot.log.Println("I'm Finished the lap!")
		bot.log.Printf("Lap time: %fs (%d ticks)", float64(lap.Data.Lap.Millis)/1000, lap.Data.Lap.Ticks)
		bot.log.Println("Ranking:")
		bot.log.Println("Rank: ", lap.Data.Rank.Overall)
		bot.log.Println("Fastest Lap: ", lap.Data.Rank.FastestLap)
	}
	bot.send_ping()
}

func (bot *Bot) onError(line string) {
	bot.log.Printf(fmt.Sprintf("Got error: %v", line))
	bot.send_ping()
}

func (bot *Bot) getSpeed(myCar msg.CarPosition) (speed, acseleration, fcent, fcent2, fcent3, fcent4 float64) {
	prevIndex := bot.lastPosition.Position.Index
	prevPosition := bot.lastPosition.Position
	currentIndex := myCar.Position.Index
	currentPosition := myCar.Position

	// If there some pervious position, calculate speed
	if prevIndex >= 0 {
		var distance, radius float64
		var count int

		// Bottom speeds
		angleSpeed := math.Abs(myCar.Angle-bot.lastPosition.Angle) * math.Pi / 180
		bottomR := (bot.myDimm.Length - bot.myDimm.Guide)
		bottomSpeed := angleSpeed * bottomR

		trackLen := bot.track.Length

		if currentIndex < prevIndex {
			// start crossed
			count = trackLen + currentIndex - prevIndex
		} else {
			count = currentIndex - prevIndex
		}

		if prevIndex == currentIndex {
			distance = currentPosition.InPiece - prevPosition.InPiece
		} else {
			for i := prevIndex; i <= prevIndex+count; i++ {
				var pieceIndex = i % trackLen
				piece := bot.track.Pieces[pieceIndex]
				lane := bot.track.Lanes[currentPosition.Lane.End]

				if pieceIndex == prevIndex {
					distance += piece.LaneLengths[lane.Index] - prevPosition.InPiece
				} else if pieceIndex == currentIndex {
					distance += currentPosition.InPiece
				} else {
					distance += piece.LaneLengths[lane.Index]
				}
			}
		}
		speed = distance
		radius = bot.track.Pieces[currentIndex].LaneRadius[myCar.Position.Lane.End]
		if radius > 0 {
			fcent = math.Pow(speed, 2) / radius
		} else {
			fcent = 0
		}
		totalBottomSpeed := math.Sqrt(math.Pow(speed, 2) + math.Pow(bottomSpeed, 2) - 2*speed*bottomSpeed*math.Cos((90-myCar.Angle)*math.Pi/180))
		fcent2 = math.Pow(totalBottomSpeed, 2) / bottomR
		fcent3 = math.Pow(angleSpeed, 2) * bottomR
		fcent4 = math.Pow(bottomSpeed, 2) / bottomR
	}
	bot.lastPosition = myCar

	if bot.speed > 0 {
		acseleration = speed - bot.speed
	}
	bot.speed = speed
	return
}

func (bot *Bot) onCarPositions(line string) {
	if !bot.crashed {
		var carPos msg.EnvelopeCarPosition
		var myCar msg.CarPosition
		err := json.Unmarshal([]byte(line), &carPos)
		if err != nil {
			bot.log.Println("Error parsing car position message", err)
			bot.send_ping()
			return
		}

		for _, car := range carPos.Data {
			if car.Id.Name == bot.Name {
				myCar = car
			}
		}
		// Caclulating speed
		speed, acseleration, fcent, fcent2, fcent3, fcent4 := bot.getSpeed(myCar)
		bot.angleForce = math.Abs(myCar.Angle * fcent)
		bot.speed = speed
		bot.acseleration = acseleration
		bot.cForce = fcent

		// Checking incomming corners
		strait, radius := bot.calculateDistanceToTurn(myCar)
		if radius > 0 {
			spd := math.Sqrt(radius * bot.MaxForce / bot.SafeSpeedModifier)
			if math.Abs(spd-bot.safeSpeed) > 0.000001 {
				bot.safeSpeed = spd
				bot.log.Printf("New safeSpeed= %f", spd)
			}
		}

		// Dumping data
		piece := bot.track.Pieces[myCar.Position.Index]
		lane := bot.track.Lanes[myCar.Position.Lane.End]
		radius = piece.LaneRadius[lane.Index]

		bot.dump.Write([]byte(fmt.Sprintf("%d,%d,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\n", myCar.Position.Lap, myCar.Position.Index, speed, bot.safeSpeed, acseleration, myCar.Angle, bot.throttle, fcent, fcent2, fcent3, fcent4, radius, piece.Angle)))

		// Calculating braking distance
		if strait > 0 {
			var distance float64
			var breakSpeed = speed
			var tiksToBrake int

			//bot.log.Println("Start calculating breaks")
			for i := 0; i < bot.FarSeer; i++ {
				distance += breakSpeed
				breakSpeed *= bot.BrakeDecseleration
				if breakSpeed <= bot.safeSpeed {
					tiksToBrake = i
					break
				}
			}

			if tiksToBrake > 0 { // Need to brake
				//bot.log.Println("Calculated braking! Brake distance: ", distance)
				if distance*1.05 >= strait {
					//bot.log.Printf("Braking! Strait: %f, speed: %f, tiksToBrake: %d", strait, speed, tiksToBrake)
					bot.throttle = bot.BrakeThrottle
					bot.send_throttle()
					return
				}
			}
		}

		if strait <= 0.1 { // On curve
			piece := bot.track.Pieces[myCar.Position.Index]

			if piece.Angle*myCar.Angle > 0 { // Dangerous angle
				if bot.angleForce > 0 {
					bot.log.Printf("AngleForce= %f; MaxForce= %f", bot.angleForce, bot.MaxForce)
					diff := ((bot.MaxForce + bot.angleForce) / bot.MaxForce)
					if bot.angleForce <= bot.MaxForce && bot.speed < bot.safeSpeed {
						bot.log.Printf("Safe! Changing throttle on %f", 0.1*diff)
						bot.throttle += 0.1 * diff
					} else {
						bot.log.Printf("Danger! Decreasing throttle on %f", 0.05*diff)
						if bot.throttle > bot.MinThrottle {
							bot.throttle -= 0.05 * diff
						}
					}
				} else {
					bot.log.Println("No angleForce!")
					bot.throttle += 0.01
				}
			} else { // Safe drift
				if speed < bot.safeSpeed {
					bot.throttle += 0.1
				}
			}
			bot.send_throttle()
			return
		}

		// On strait
		bot.throttle = 1
		bot.send_throttle()
	} else {
		bot.send_ping()
	}
}

func (bot *Bot) parseMessage(line string) {
	var env msg.EnvelopeHeader
	err := json.Unmarshal([]byte(line), &env)
	if err != nil {
		bot.log.Println("Error parsing message", err)
		return
	}

	switch env.Type {
	case "join":
		bot.onJoin()
	case "gameStart":
		bot.onGameStart()
	case "gameInit":
		bot.onGameInit(line)
	case "yourCar":
		bot.onYourCar(line)
	case "carPositions":
		bot.onCarPositions(line)
	case "crash":
		bot.onCrash(line)
	case "spawn":
		bot.onSpawn(line)
	case "gameEnd":
		bot.onGameEnd(line)
	case "lapFinished":
		bot.onLapFinish(line)
	case "error":
		bot.onError(line)
	default:
		bot.log.Printf("Got message [%s]", env.Type)
		bot.send_ping()
	}
}

func (bot *Bot) Run() (err error) {
	bot.log = log.New(os.Stdout, "", log.Ldate|log.Ltime|log.Lmicroseconds|log.Lshortfile)
	t := time.Now()
	bot.dump, err = os.Create(fmt.Sprintf("data/data%d.%d-%d:%d.csv", t.Month(), t.Day(), t.Hour(), t.Minute()))
	if err != nil {
		bot.log.Println("Error opening the file!")
	}
	defer func() {
		if err := bot.dump.Close(); err != nil {
			panic(err)
		}
	}()
	bot.dump.WriteString(fmt.Sprintf("\"MaxForce:\",%f,\"brakeThrottle:\",%f,\"minThrottle:\",%f\n", bot.MaxForce, bot.BrakeThrottle, bot.MinThrottle))
	bot.log.Printf("MaxForce: %f, brakeThrottle: %f,minThrottle: %f", bot.MaxForce, bot.BrakeThrottle, bot.MinThrottle)
	bot.log.Printf("safeSpeedModifier: %f, farSeer: %d, brakeDecseleration: %f", bot.SafeSpeedModifier, bot.FarSeer, bot.BrakeDecseleration)
	bot.dump.Write([]byte("\"Lap\",\"Piece\",\"speed\",\"safe spee\",\"acs\",\"angle\",\"throttle\",\"fcent\",\"fcent2\",\"fcent3\",\"fcent4\",\"Radius\",\"piceAngle\"\n"))
	if bot.Create {
		bot.send_create()
	} else {
		bot.send_join()
	}
	for {
		input, err := bot.read_msg()
		if err != nil {
			bot.log.Fatal(err, "Reading message")
		}
		bot.parseMessage(input)
	}
}

package msg

type CarId struct {
    Name  string
    Color string
}

type CarDimmensions struct {
    Length float64
    Width  float64
    Guide  float64
}

type Car struct {
    Id         CarId
    Dimensions CarDimmensions
}

type CarPosition struct {
    Id       CarId
    Angle    float64
    Position PiecePosition `json:"piecePosition"`
}

type CarResult struct {
    Car    CarId
    Result RaceTime
}

type RaceTime struct {
    Laps   int
    Ticks  int
    Millis int
}

type CarBestLap struct {
    Car    CarId
    Result LapTime
}

type LapTime struct {
    Lap    int
    Ticks  int
    Millis int
}

type RaceWrap struct {
    Race Race
}

type Race struct {
    Track Track
    Cars  []Car
    Info  RaceInfo `json:"raceSession"`
}

type RaceInfo struct {
    Laps       int
    MaxLapTime int  `json:"maxLapTimeMs"`
    Quick      bool `json:"quickRace"`
}

type PiecePosition struct {
    Index   int     `json:"pieceIndex"`
    InPiece float64 `json:"inPieceDistance"`
    Lane    RaceLane
    Lap     int
}

type Piece struct {
    Length      float64
    Switch      bool
    Radius      float64
    Angle       float64
    LaneLengths map[int]float64
    LaneRadius  map[int]float64
}

type RaceLane struct {
    Start int `json:"startLaneIndex"`
    End   int `json:"endLaneIndex"`
}

type TrackLane struct {
    Offset float64 `json:"distanceFromCenter"`
    Index  int
}

type Coords struct {
    X   float64
    Y   float64
}

type StartPosition struct {
    Coords Coords `json:"position"`
    Angle  float64
}

type Track struct {
    Id     string
    Name   string
    Pieces []Piece
    Length int
    Lanes  []TrackLane
    SP     StartPosition `json:"startPosition"`
}

type Crash struct {
    Name  string
    Color string
}

type Spawn struct {
    Name  string
    Color string
}

type Ranking struct {
    Overall    int
    FastestLap int
}

type LapFinish struct {
    Car  CarId
    Lap  LapTime  `json:"lapTime"`
    Race RaceTime `json:"raceTime"`
    Rank Ranking  `json: ranking`
}

type EndGame struct {
    Results  []CarResult
    BestLaps []CarBestLap
}

/// Envelopes ???
type GameInit struct {
    Race Race
}

type EnvelopeHeader struct {
    Type string `json:"msgType"`
    Tick int    `json:"gameTick"`
    Id   string `json:"gameId"`
}

type EnvelopeCrash struct {
    EnvelopeHeader
    Data Crash
}

type EnvelopeSpawn struct {
    EnvelopeHeader
    Data Spawn
}

type EnvelopeCarPosition struct {
    EnvelopeHeader
    Data []CarPosition
}

type EnvelopeGameStart struct {
    EnvelopeHeader
    Data interface{}
}

type EnvelopeGameInit struct {
    EnvelopeHeader
    Data RaceWrap
}

type EnvelopeGameEnd struct {
    EnvelopeHeader
    Data EndGame
}

type EnvelopeYourCar struct {
    EnvelopeHeader
    Data CarId
}

type EnvelopeLapFinish struct {
    EnvelopeHeader
    Data LapFinish
}

type BotId struct {
    Name string `json:"name"`
    Key  string `json:"key"`
}

type MsgCreate struct {
    BotId BotId  `json:"botId"`
    Track string `json:"trackName"`
    Pass  string `json:"password"`
    Cars  int    `json:"carCount"`
}
